export const labelActivation = 'Activation';
export const labelIdentityProvider = 'Identity provider';
export const labelClientAddresses = 'Client addresses';
export const labelAutoImport = 'Auto import';
export const labelPressEnterToAccept = 'Press Enter to accept';
